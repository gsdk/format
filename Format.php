<?php

namespace Gsdk\Format;

use Illuminate\Support\Facades\Facade;

/**
 * @method static string number(int|float $value, string $format = null)
 * @method static string date(mixed $value, string $format = null)
 * @method static string enum(mixed $value, string $format = null)
 * @method static string boolean(mixed $value, string $format = null)
 * @method static string fileSize(int $value, string $format = null)
 * @method static string getFormat(?string $alias)
 * @method static bool hasExtension(string $name)
 * @method static void registerFormat(string $alias, string $format)
 * @method static void registerFormats(array $formats)
 */
class Format extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'format';
    }
}