<?php

namespace Gsdk\Format\Rules;

use DateTimeInterface;
use Gsdk\Format\Format;
use Illuminate\Support\DateFactory;

class Date implements RuleInterface
{
    protected static function dateFactory($date)
    {
        $factory = new DateFactory();
        if ($date instanceof DateTimeInterface) {
            return $factory->createFromTimestamp($date->getTimestamp());
        } else {
            if (is_numeric($date)) {
                $date = $factory->createFromTimestamp($date);
            } else {
                if (is_string($date)) {
                    $date = $factory->parse($date);
                } else {
                    return null;
                }
            }
        }

        $date->setTimezone(new \DateTimeZone(date_default_timezone_get()));

        return $date;
    }

    public function format($value, $format = null): string
    {
        $date = $this->dateFactory($value);
        if (null === $date) {
            return '';
        }
        $format = Format::getFormat($format ?? 'date');

        return $date->format($format);
    }
}
