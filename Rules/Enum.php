<?php

namespace Gsdk\Format\Rules;

class Enum implements RuleInterface
{
    public function format($value, $format = null): string
    {
        $name = basename(str_replace('\\', '/', $value::class));
        if (str_ends_with($name, 'Enum')) {
            $name = substr($name, 0, -4);
        }

        return __('enum.' . strtolower($name) . '.' . strtolower($value->name));
    }
}